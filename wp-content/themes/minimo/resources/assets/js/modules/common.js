var common = {
    init: function () {

        $(".main").mousewheel(function(event, delta) {

            this.scrollLeft -= (delta);

            event.preventDefault();

        });
        // common.scroll.init();
        common.events.init();
    },

    events: {
      init: function () {

        $(window).on("scroll", function () {
            if ($(this).scrollTop() > 100) {
                $("header").addClass("dark-header");
            }
            else {
                $("header").removeClass("dark-header");
            }
        });

        // $('a[href^="#"]').on('click', function(event) {
        //   var target = $(this.getAttribute('href'));
        //   if( target.length ) {
        //       event.preventDefault();
        //       $('html, body').stop().animate({
        //           scrollTop: target.offset().top
        //       }, 1000);
        //   }
        // });

        /* Mobile hamburger */
        $('.hamburger').click( function (e) {
          e.stopImmediatePropagation();
          $(this).toggleClass('is-active');
          $('header').toggleClass('menu-open');
          $('html').toggleClass('no-scroll');
          $('.sidebar').toggleClass('active-menu');

        });

      }


    },

    scroll: {

        sticky: false,
        topbar: 0,
        last: 0,

        init: function () {
            common.scroll.do();

            $(window).unbind('scroll').scroll(function () {
                common.scroll.do();

            });
        },

        do: function () {

            var st = $(window).scrollTop();
            if($('body').hasClass('admin-bar')){
              st = $(window).scrollTop() + 32;
              common.scroll.topbar = 32;
            }

            if (st >= $('header').offset().top && st !== common.scroll.topbar) {

                common.scroll.sticky = true;
                $('header').addClass('header--scrolled');

            } else if (st == common.scroll.topbar) {

                common.scroll.sticky = false;
                $('header').removeClass('header--scrolled');
            }

            common.scroll.last = st;
            common.checkVisibleElements();
        }
      },

      setCookie: function(cookie_name, value)
      {
          var exdate = new Date();
          exdate.setDate(exdate.getDate() + (30));
          document.cookie = cookie_name + "=" + escape(value) + "; expires="+exdate.toUTCString() + "; path=/";
      },

      getCookie: function(cookie_name)
      {
          if (document.cookie.length>0)
          {
              cookie_start = document.cookie.indexOf(cookie_name + "=");
              if (cookie_start != -1)
              {
                  cookie_start = cookie_start + cookie_name.length+1;
                  cookie_end = document.cookie.indexOf(";",cookie_start);
                  if (cookie_end == -1)
                  {
                      cookie_end = document.cookie.length;
                  }
                  return unescape(document.cookie.substring(cookie_start,cookie_end));
              }
          }
          return "";
      },

      checkVisibleElements: function(elements){
            var windowScrolled = $(window).scrollTop() + $(window).height();
            var index = 0;

            $('.animate:not(.animate--visible, .animate--waiting)').each(function(i, element){
                if($(element).offset().top < windowScrolled){
                    setTimeout(function(){
                        $(element).addClass('animate--visible').removeClass('animate--waiting');
                    }, $(element).index() * 100 );
                    $(element).addClass('animate--waiting');
                }
            });
        },
}
common.init();
