var cashcarry = {
    init: function () {
    	require(['async!https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyCXCYNdBKHz7wJEJO0KHVkqr-MsSDBezk8'], function() {
            cashcarry.maps( $('#map-wrap') );
        });
    },
    maps: function(elem) {
    	 this.elem = elem;
            var self = this;
            var mapDiv = this.elem[0];

            if ( $(mapDiv).data('zoom') ) {
                self.zoom = parseInt($(mapDiv).data('zoom'));
            }

            if( $(mapDiv).data('pin') ) {
                self.pin = $(mapDiv).data('pin');
            }

            if( $(mapDiv).data('pin2') ) {
                self.pin2 = $(mapDiv).data('pin2');
            }

            var map = new google.maps.Map(mapDiv, {
                center: {lat: $(mapDiv).data('lat'), lng: $(mapDiv).data('lng')},
                zoom: self.zoom,
                disableDefaultUI: true,
                mapTypeControl: false,
                scrollwheel: false,
                draggable: true,
                zoomControl: true,
          		fullscreenControl: true,
          		streetViewControl: true,
                styles: [
						  {
						    "elementType": "geometry",
						    "stylers": [
						      {
						        "color": "#212121"
						      }
						    ]
						  },
						  {
						    "elementType": "labels.icon",
						    "stylers": [
						      {
						        "visibility": "off"
						      }
						    ]
						  },
						  {
						    "elementType": "labels.text.fill",
						    "stylers": [
						      {
						        "color": "#757575"
						      }
						    ]
						  },
						  {
						    "elementType": "labels.text.stroke",
						    "stylers": [
						      {
						        "color": "#212121"
						      }
						    ]
						  },
						  {
						    "featureType": "administrative",
						    "elementType": "geometry",
						    "stylers": [
						      {
						        "color": "#757575"
						      }
						    ]
						  },
						  {
						    "featureType": "administrative.country",
						    "elementType": "labels.text.fill",
						    "stylers": [
						      {
						        "color": "#9e9e9e"
						      }
						    ]
						  },
						  {
						    "featureType": "administrative.land_parcel",
						    "stylers": [
						      {
						        "visibility": "off"
						      }
						    ]
						  },
						  {
						    "featureType": "administrative.locality",
						    "elementType": "labels.text.fill",
						    "stylers": [
						      {
						        "color": "#bdbdbd"
						      }
						    ]
						  },
						  {
						    "featureType": "poi",
						    "elementType": "labels.text.fill",
						    "stylers": [
						      {
						        "color": "#757575"
						      }
						    ]
						  },
						  {
						    "featureType": "poi.park",
						    "elementType": "geometry",
						    "stylers": [
						      {
						        "color": "#181818"
						      }
						    ]
						  },
						  {
						    "featureType": "poi.park",
						    "elementType": "labels.text.fill",
						    "stylers": [
						      {
						        "color": "#616161"
						      }
						    ]
						  },
						  {
						    "featureType": "poi.park",
						    "elementType": "labels.text.stroke",
						    "stylers": [
						      {
						        "color": "#1b1b1b"
						      }
						    ]
						  },
						  {
						    "featureType": "road",
						    "elementType": "geometry.fill",
						    "stylers": [
						      {
						        "color": "#2c2c2c"
						      }
						    ]
						  },
						  {
						    "featureType": "road",
						    "elementType": "labels.text.fill",
						    "stylers": [
						      {
						        "color": "#8a8a8a"
						      }
						    ]
						  },
						  {
						    "featureType": "road.arterial",
						    "elementType": "geometry",
						    "stylers": [
						      {
						        "color": "#373737"
						      }
						    ]
						  },
						  {
						    "featureType": "road.highway",
						    "elementType": "geometry",
						    "stylers": [
						      {
						        "color": "#3c3c3c"
						      }
						    ]
						  },
						  {
						    "featureType": "road.highway.controlled_access",
						    "elementType": "geometry",
						    "stylers": [
						      {
						        "color": "#4e4e4e"
						      }
						    ]
						  },
						  {
						    "featureType": "road.local",
						    "elementType": "labels.text.fill",
						    "stylers": [
						      {
						        "color": "#616161"
						      }
						    ]
						  },
						  {
						    "featureType": "transit",
						    "elementType": "labels.text.fill",
						    "stylers": [
						      {
						        "color": "#757575"
						      }
						    ]
						  },
						  {
						    "featureType": "water",
						    "elementType": "geometry",
						    "stylers": [
						      {
						        "color": "#000000"
						      }
						    ]
						  },
						  {
						    "featureType": "water",
						    "elementType": "labels.text.fill",
						    "stylers": [
						      {
						        "color": "#3d3d3d"
						      }
						    ]
						  }
						]
            });

            var bounds = new google.maps.LatLngBounds();

            var center = new google.maps.LatLng($(mapDiv).data('lat'), $(mapDiv).data('lng'));
            var marker_options = {
                position: center,
                map: map,
                animation: google.maps.Animation.DROP
            }
            if( self.pin ) {
                marker_options.icon = self.pin;
            }
            var marker = new google.maps.Marker(marker_options);
            bounds.extend(center);

            var center = new google.maps.LatLng($(mapDiv).data('lat2'), $(mapDiv).data('lng2'));
            var marker_options = {
                position: center,
                map: map,
                animation: google.maps.Animation.DROP
            }
            if( self.pin2 ) {
                marker_options.icon = self.pin2;
            }
            var marker2 = new google.maps.Marker(marker_options);
            bounds.extend(center);

            map.panToBounds(bounds);
    }
}

cashcarry.init();