var interior = {


    init : function () {
      console.log("Hello");

      $('.post-item__gallery').slick({
        arrows: false,
        dots: true,
        draggable: true,
        slidesToShow: 1,
      });

      $( ".info-button" ).hover(
        function() {
          var id = this.getAttribute("data-info-id");
          $('.post-item__description[data-id="'+id+'"]').addClass( "active" );
        }, function() {
          var id = this.getAttribute("data-info-id");
          $('.post-item__description[data-id="'+id+'"]').removeClass( "active" );
        }
      );

    },

}
interior.init();
