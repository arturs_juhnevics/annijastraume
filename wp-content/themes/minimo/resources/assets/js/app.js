requirejs.config({
    baseUrl: base_url + '/assets/js/modules',
    urlArgs: "v=1.23.0",
    waitSeconds : 30,
    paths: {

        // plugins
        'async'             : '../plugins/async',
        'slick'             : '../plugins/slick.min',
        'jquery'            : '../plugins/jquery3.3.1.min',
        'selectize'         : '../plugins/selectize.min',
        'bootstrap'         : '../plugins/bootstrap.bundle.min',
        'mousewheel'        : '../plugins/mousewheel.min',
        'columnizer'        : '../plugins/jquery.columnizer',
        'photoswipe'     : '../plugins/photoswipe.min',
        'photoswipeUI'   : '../plugins/photoswipe-ui-default.min',
        'photoswipe_obj' : 'min/photoswipe',


        //'cooqui'         : '../plugins/jquery-cabg-cooqui',
        //'pagescroll'         : '../plugins/jquery.onepage-scroll.min',


        // modules
        'common'                            : 'min/common',
        'page_template_template_contacts'   : 'min/contacts',
        'page_template_template_home'       : 'min/home',
        'page_template_template_interior'   : 'min/interior',
        'page_template_template_cashcary'   : 'min/cashcary',
        'single_product'                    : 'min/product',
        'single_stasti'                    : 'min/stories',


    },

    shim: {
        'common': {
            deps: ['async', 'jquery', 'slick', 'bootstrap', 'mousewheel', ]
        },
        'page_template_template_contacts' : {
            deps : ['async']
        },
        'page_template_template_cashcary' : {
            deps : ['async']
        },
        'page_template_template_interior': {
            deps: ['slick']
        },
        'page_template_template_product': {
            deps: ['slick']
        },
        'page_template_template_home': {
            deps: ['async']
        },
        'single_stasti': {
          deps: ['async', 'columnizer']
      },

    }
});

requirejs(['common'], function() {
    (function($) {
        var UTIL = {
            loadEvents: function() {
                common.init();
                $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                    if (typeof(requirejs.s.contexts._.config.paths[classnm]) != 'undefined') {
                        requirejs([classnm], function() {

                            if (typeof(this[classnm]) !== 'undefined') {
                                if (typeof(this[classnm].finalize) !== 'undefined') {
                                    this[classnm].finalize();
                                }
                            }
                        });
                    }
                });
            }
        };
        // Load Events
        $(document).ready(UTIL.loadEvents);
    })(jQuery); // Fully reference jQuery after this point.
});
