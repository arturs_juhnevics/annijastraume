{{--
  Template Name: Home
--}}
@extends('layouts.app')
@section('content')

<?php 

?>

  @while(have_posts()) @php the_post() @endphp
    @include('partials.home.interior')
  @endwhile
@endsection