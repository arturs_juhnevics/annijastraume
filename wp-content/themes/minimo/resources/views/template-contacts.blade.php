{{--
  Template Name: Contacts
--}}
@extends('layouts.app')
@section('content')
<?php
  $name = rwmb_meta('contact_name', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $email = rwmb_meta('contact_email', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $phone = rwmb_meta('contact_phone', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');

  $images = rwmb_meta( 'contact_image', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $image = reset( $images );
  $full_image = $image['full_url'];

?>
  @while(have_posts()) @php the_post() @endphp

    <div class="contacts container">
       <p class="contacts-item"> <?php echo $name; ?> </p>
       <a  class="contacts-item" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
       <a  class="contacts-item" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
    </div>

  @endwhile

@endsection
