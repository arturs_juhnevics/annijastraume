@extends('layouts.app')

@section('content')
@include('layouts.page-header-simple')
<?php
$description = rwmb_meta( 'p_archive_text', array( 'object_type' => 'setting'), 'settings');
?>
<div class="container products">
	<div class="row">
  		@while(have_posts()) @php the_post() @endphp
  			<?php 
  			$image = get_the_post_thumbnail_url();
			$title = get_the_title(); 
			$url = get_the_permalink();
  			?>
  			<div class="col-sm-4">
  				<a href="{{ $url }}">
					<div class="product-item--medium product-item animate animate__fade" style="background-image: url({{ $image }})">
						<div class="overlay"></div>
						<div class="button-overlay"><p class="button--read-more">VIEW PRODUCT</p></div>
						<p class="product-item__title">{{ $title }}</p>
					</div>
				</a>
  			</div>
	   @endwhile
	</div>
</div>
@endsection