@extends('layouts.app')
@section('content')
<?php
$info = rwmb_meta('interiot_info_list');  
?>
<?php if ( have_posts() ) : the_post(); ?>
    <div class="single-post-item">

        <div class="single-post-item__intro">
            <div class="single-post-item__image">
                <img alt="<?php echo the_title(); ?>" src="<?php echo get_the_post_thumbnail_url() ?>"/>
            </div>
            <div class="single-post-item__info">
                <h2 class="single-post-item__info__title"><?php echo the_title(); ?></h2>
                <div class="info-list">

                    <?php foreach ($info as $item) : ?>
                        <p class="info-list__item"><?php echo $item[0]; ?>: <?php echo $item[1]; ?></p>
                    <?php endforeach; ?>
                    
                </div>
            </div>
        </div>

        <div class="single-post-item__content">
            <?php the_content(); ?>
        </div>
    
    </div>
<?php endif; ?>
@endsection


   			