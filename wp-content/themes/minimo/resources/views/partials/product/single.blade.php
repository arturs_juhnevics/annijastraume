<?php 
$gallery = rwmb_meta( 'product_gallery', array('size' => 'medium') );
$term = wp_get_post_terms( get_the_ID(), 'brand-category');
$brandLogo = rwmb_meta( 'brand_cat_image', array( 'object_type' => 'term', 'limit' => 1 ), $term[0]->term_id );
reset($brandLogo);

$termCategory = wp_get_post_terms( get_the_ID(), 'product-category');
$categoryUrl = get_term_link($termCategory[0]->term_id);

$id = get_the_ID();
global $wpdb;
$solutions = $wpdb->get_results( $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_value = %d AND meta_key = %s", $id, 'solution_product') );	
?>
<div class="container">
	<div class="row">
		<div class="col-sm-6">
			<div class="gallery">
				<div class="gallery__main">
					<div class="gallery__main__inner">
						<?php foreach ($gallery as $image) { ?>
							<div class="gallery__main__item">
								<div class="gallery-image" style="background-image:url(<?php echo $image['url'] ?>);">
								</div>
							</div>
						<?php } ?>
						
					</div>
				</div>
				<?php 
					$gallery = rwmb_meta( 'product_gallery', array('size' => 'medium') );
				?>
				<div class="gallery__thumbs">
					<div class="gallery__thumbs__inner">
						<?php foreach ($gallery as $image) { ?>
							<div class="gallery__thumbs__item">
								<img src="<?php echo $image['url'] ?>" />
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="single-product__info">
				<img src="<?php echo $brandLogo[0]['url']; ?>">
				<p class="single-product__info__item"><span><?php echo pll__("Kategorija:"); ?></span><a class="link link--grey" href="<?php echo $categoryUrl; ?>"><?php echo $termCategory[0]->name; ?></a></p>
				<p class="single-product__info__item"><span><?php echo pll__("Risinājumi:"); ?></span>
					<?php foreach ($solutions as $value) { ?>
						<?php 
							$url = get_the_permalink( $value->post_id );
							$title = get_the_title( $value->post_id )
						?>
						<a class="link link--grey" href="<?php echo $url; ?>"><?php echo $title; ?></a>
					<?php } ?>
					
				</p>
				<hr>
				<p class="single-product__info__text">
					Īpaši efektīvi tīrītājdiski ЗМ™ Cubitron™ II ir izstrādāti visu veidu metālu un sakausējumu apstrādei: melnais tērauds, nerūsējošais tērauds, čuguns, u.c. Priekšrocības: palielina produktivitāti, rekordaugsts apstrādes ātrums, Ilgāk saglabā slīpēšanas agresivitāti, samazina kopējo tēriņu apmēru
				</p>
			</div>
		</div>
	</div>
<div class="accordion product-extra" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <?php echo pll__("Risinājumi"); ?>
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        <div class="row">
        	<?php foreach ($solutions as $value) { ?>
				<?php 
					$url = get_the_permalink( $value->post_id );
					$title = get_the_title( $value->post_id );
					$image = get_the_post_thumbnail_url( $value->post_id );
					$short = rwmb_meta('solution_short_text', array('size' =>'medium'), $value->post_id);
					$trimmed = wp_trim_words( $short, 30);
				?>
				<div class="col-sm-6">
					<div class="product-extra__solution-image">
						<img src="<?php echo $image; ?>">
					</div>
					<div class="product-extra__text">
						<h3><?php echo $title; ?></h3>
						<p><?php echo $trimmed ; ?></p>
						<a class="readmore-text" href="<?php $url; ?>"><?php echo pll__("Lasīt vairāk"); ?></a>
					</div>
				</div>
			<?php } ?>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          <?php echo pll__("Papildus"); ?>
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
      	<div class="text">
       		<?php echo rwmb_meta('product_extra'); ?>
       </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <?php echo pll__("Tehniskie parametri"); ?>
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
      		<div class="text">
      			<?php echo rwmb_meta('product_tech'); ?>
      		</div>
        	
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFour">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <?php echo pll__("Lejuplādes"); ?>
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse show" aria-labelledby="headingFour" data-parent="#accordionExample">
      <div class="card-body">
     	<?php 
     		$downloads = rwmb_meta('product_files');
     	?>
     	<?php foreach ($downloads as $value) { ?>
     	<?php 
     		$ext = pathinfo($value['path'], PATHINFO_EXTENSION); 
     		$extname = 'file';
     		$fileClass = 'regular';
     		if($ext == 'xls'){
				$extname = 'xls';
     		}
     	?>
     		<a href="<?php echo $value['url']; ?>" target="_blank" class="downloads__item <?php echo $fileClass; ?>">
     			<span class="downloads__item__ext"><?php echo $extname; ?></span>
     			<span class="downloads__item__name"><?php echo $value['name']; ?></span>
     			<span class="downloads__item__size">1.2 MB</span>
     		</a>
     	<?php } ?>
      </div>
    </div>
  </div>
</div>
	<div class="related-products">
		<h2>Saistītie prodokti</h2>
		<div class="row">
			<?php 
				$args = array(
				'post_type'        => 'product',
				'posts_per_page'   => 4,
			
				);
				$query = new WP_Query( $args ); 
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
					$query->the_post(); ?>
					
					<div class="col-sm-3">
						@include('partials.product.product-list') 
					</div>
					<?php } // end while
				} // end if
				wp_reset_query();
			?>
		</div>
	</div>
</div>