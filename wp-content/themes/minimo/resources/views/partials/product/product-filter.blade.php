<?php
$taxonomyBrands = get_taxonomy( 'brand-category');
$termsBrands = get_terms( 'brand-category', array(
    'taxonomy' => get_queried_object()->name,
    'hide_empty' => false,
) );
global $post;
$args = array('post_type' => 'solution', 'posts_per_page' => 5);
$solutions = get_posts( $args );
?> 

<div class="product-filter">
<div class="product-filter__heading">
  <p class="product-filter__heading__title">Atlasīt preces</p>
  <a class="product-filter__heading__text button--light">Atcelt visus</a>
</div>
<div class="accordion product-filter__inner" id="product-filter">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#brands" aria-expanded="true" aria-controls="brands">
          Ražotāji
        </button>
      </h5>
    </div>

    <div id="brands" class="collapse show brands" aria-labelledby="headingOne" data-parent="#product-filter">
      <div class="card-body product-filter__contents">
      	<?php foreach ($termsBrands as $item) { ?>
          <div class="product-filter__contents__item">
              <input type="checkbox" value="<?php echo $item->slug; ?>" id="<?php echo $item->term_id; ?>" >
              <label for="<?php echo $item->term_id; ?>"><?php echo $item->name; ?> (<?php echo $item->count; ?>)</label>
          </div>
      	<?php } ?>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#brands" aria-expanded="true" aria-controls="brands">
          Risinājumi
        </button>
      </h5>
    </div>

    <div id="brands" class="collapse show brands" aria-labelledby="headingOne" data-parent="#product-filter">
      <div class="card-body product-filter__contents">
        <?php foreach ($solutions as $post) : ?>
          <div class="product-filter__contents__item">
              <input name="brand" type="radio" value="<?php the_title(); ?>" id="<?php the_title(); ?>" >
              <label for="<?php the_title(); ?>"><?php the_title(); ?> ()</label>
          </div>
        <?php endforeach; 
        wp_reset_postdata();?> 
      </div>
    </div>
  </div>
</div>
</div>