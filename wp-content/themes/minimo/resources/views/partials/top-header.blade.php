@php
$logos = rwmb_meta( 'logo', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo = reset( $logos );
$header_image = $logo['full_url'];

$logos2 = rwmb_meta( 'logo2', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo2 = reset( $logos2 );
$header_image2 = $logo2['full_url'];

$lang_args = array(
	'raw'=>1
);
$classWPbar = "top-header";
if ( is_admin_bar_showing() ) {
  $classWPbar = "top-header wp-bar";
}
@endphp

<div class="{{$classWPbar}}">
  <div class="header-inner">

    <div class="header-inner--item">
      <a class="navbar-brand" href="/">
        <img
          class="main-logo"
          alt="Annija Straume Logo"
          src="<?php echo $header_image; ?>"
        />
      </a>
    </div>
    <div class="header-inner--item">
      <img
        alt="Annija Straume"
        src="<?php echo $header_image2; ?>"
        class="text-logo"
      />
      </a>
    </div>
    <div class="header-inner--item"></div>
  </div>
</div>
