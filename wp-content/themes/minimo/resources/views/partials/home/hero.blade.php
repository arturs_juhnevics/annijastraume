@php
$slides = get_posts( array(
    'post_type' => 'slider',
    'numberposts' => -1,
    'post_status' => 'publish',
) );

@endphp
<div class="hero-wrapper">
	<div class="hero">
	@foreach ($slides as $slide)
		@php
		$image = get_the_post_thumbnail_url($slide->ID);

		$title = get_post_meta($slide->ID, 'slider_title');
		$text = get_post_meta($slide->ID, 'slider_text');
		$buttonText = get_post_meta($slide->ID, 'button_text');
		$buttonUrl = get_post_meta($slide->ID, 'button_url');
		$full_width = get_post_meta($slide->ID, 'full_width_bg');
		$full = '';
		if ( $full_width[0] == '1' ) {
			$full = 'full-width';
		}

		@endphp
		<div class="hero__item animate {{ $full }}">
			<div class="overlay"></div>
			<img class="hero__bg" data-lazy="{{ $image }}"/>
			<div class="container hero__content ">
	            <div class="hero__item__inner">
	            	<h2 class="hero__title">{{ $title[0] }}</h2>
	            		@if($text)
	                    	<div class="hero__text">{{ $text[0] }}</div>
	                    @endif
	                    @if($buttonUrl)
						<a href="{{ $buttonUrl[0] }}" class="button">{{ $buttonText[0] }}</a> 
						@endif
	           	</div>
	        </div>
		</div>
	@endforeach
	</div>
	<div class="slider-dots container mob-hidden "></div>
	<div class="hero-socials desktop-only">
		<div class="container">
			
		
		<?php
        $platforms = array('twitter','youtube', 'facebook', 'linkedin', 'instagram', 'draugiem', 'google');
        ?>
        <ul class="socials animate animate__fade-up">
        	<li><?php echo pll_e('Seko mums', 'General') ?></li>
            <?php foreach ($platforms as $pl) : $_pl = rwmb_meta($pl, array( 'object_type' => 'setting',  'limit' => 1), 'settings');?>
                <?php if(!empty($_pl)) : ?>
                    <li class="social">
                        <a class="soc-item" target="_blank" href="<?php echo esc_url($_pl); ?>">
                        	<span class="social__icon"><?php echo file_get_contents(get_template_directory().'/assets/images/'.$pl.'.svg'); ?></span>                  
                        </a>
                    </li>
                    <?php
                endif;
            endforeach; ?>
        </ul>
        </div>
	</div>
</div>
