@php
$term = get_the_category(get_the_ID());
@endphp
<div class="col-sm-4">
	<a class="blog-item-a" href="{{ get_permalink() }}">
	<article @php post_class('blog-item') @endphp>
		
		<div class="blog-item__image animate animate__fade-up" style="background-image: url({{ get_the_post_thumbnail_url(get_the_ID(), 'medium') }});">
		</div>
		<p class="blog-item__category animate animate__fade-up">{{ $term[0]->name }}</p>
	    <h2 class="blog-item__title animate animate__fade-up"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h2>
	    
	</article>
	</a>
</div>

