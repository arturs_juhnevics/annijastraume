<footer>
	<div class="footer-content">
		<div class="menu animate">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
    </div>
	</div>
</footer>
