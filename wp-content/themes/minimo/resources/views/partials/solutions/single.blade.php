<div class="entry-content">
	 @while(have_posts()) @php the_post() @endphp
	 	<div class="entry-content__featured-image" style="background-image: url(<?php echo get_the_post_thumbnail_url();  ?>)" >
	 	</div>
		<?php echo get_the_content(); ?>
	 @endwhile


</div>
<?php $products = rwmb_meta('solution_product');?>
<div class="container">
	<div class="related-products">
		<h2>Saistītie prodokti</h2>
		<div class="row">
			<?php 
				$args = array(
				'post_type'        => 'product',
				'posts_per_page'   => 4,
				'post__in' => $products
				);
				$query = new WP_Query( $args ); 
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
					$query->the_post(); ?>
					
					<div class="col-sm-3">
						@include('partials.product.product-list') 
					</div>
					<?php } // end while
				} // end if
				wp_reset_query();
			?>
		</div>
	</div>
</div>