@extends('layouts.app')
@section('content')

<?php if ( have_posts() ) : the_post(); ?>
    <?php
    $story_gallery = rwmb_meta('story_gallery');

    ?>
    <div class="single-post-item">
      <div class="stasti-header"><span  onclick="history.back()" class="close"></span></div>
        <div class="single-post-sidebar">
        <h1 class="stasti-title">{{the_title()}}</h2>
          <p class="stasti-footer">Annija Straume <br> buvlaukums.lv</p>
        </div>
        <div class="single-post-content">
          <div class="single-post-gallery">
            <?php foreach ($story_gallery as $item) : ?>
              <div class="single-post-gallery-item">
                <img src="<?php echo wp_get_attachment_image_src($item["story_image"][0], 'medium')[0]; ?>"/>
                <?php if($item["text"][0]) : ?>
                  <p class="single-post-gallery-title"><?php echo $item["text"][0]; ?></p>
                <?php endif; ?>
              </div>
            <?php endforeach; ?>
          </div>
          <div class="single-post-item__content single-post-item__content-stories">
            <p>
            <?php
              echo wp_strip_all_tags( get_the_content() );
            ?>
            </p>
          </div>
        </div>
    </div>
<?php endif; ?>
@endsection
