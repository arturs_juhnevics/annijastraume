  <!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>

  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '468693844077660',
        cookie     : true,
        xfbml      : true,
        version    : 'v5.0'
      });

      FB.AppEvents.logPageView();


    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));


  </script>

    @php do_action('get_header') @endphp

      <div class="wrapper">
        @include('partials.mobile-header')
        <div class="sidebar">
          @include('partials.header')
        </div>
        @include('partials.top-header')
        <div class="main">

            @yield('content')

        </div>
        @include('partials.footer')
      </div>
    @php do_action('get_footer') @endphp
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins/rellax.min.js"></script>
     <script async data-main="<?php echo get_template_directory_uri(); ?>/assets/js/app.js?v=1.1.1"
                src="<?php echo get_template_directory_uri(); ?>/assets/js/require.min.js"></script>
    @php wp_footer() @endphp
</html>
