@extends('layouts.app')
@section('content')

 <?php

    $args = array(
        'post_type' => 'stasti',
        'post_status' => 'publish',
        'posts_per_page' => 20,
    );

    $loop = new WP_Query( $args );
  ?>

    <div class="post-content d-sm-flex">
          <div class="stories-page-intro">
            <h1 class="stories-page-intro-title">In the circle of current event in contemporary design.</h1>
            <p class="stories-page-intro-text">Studio’s founder Annija Straume occasionaly writes educational pieces on symbiosis of design, purposeful inovations and nature, giving a glimpse into an in-depth understanding of design to both professionals of the field and curious passers by.</p>
          </div>
          <?php $counter = 0 ?>

      <?php while ( $loop->have_posts() ) : $loop->the_post();  ?>
        <?php
          $offsetTop = rwmb_meta('stories_offset');
          $subtitle = rwmb_meta('stories_subtitle');
        ?>
        <a class="link-wrapper" href="<?php echo get_the_permalink(); ?>">
          <div class="post-item post-item-stories <?php echo (($counter % 2 == 0) ? 'low' : 'high') ?>">
            <div class="post-item__image">
              <img alt="<?php echo the_title(); ?>" src="<?php echo get_the_post_thumbnail_url() ?>"/>
            </div>
            <div class="post-item__content post-item__content-stories">
                <div class="post-item__info">
                    <h2 class="post-item__content__title"><?php echo the_title(); ?></h2>
                    <p class="post-item__content__subtitle"><?php echo $subtitle; ?></p>
                    <a class="post-item__content-stories-read" href="{{ get_the_permalink() }}">Read</a>
                </div>
            </div>
          </div>
        </a>
        <?php $counter++; ?>
      <?php endwhile; ?>

    </div>

  <?php
    wp_reset_postdata();
 ?>
@endsection
