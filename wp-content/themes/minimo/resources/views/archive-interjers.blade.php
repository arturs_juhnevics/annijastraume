@extends('layouts.app')
@section('content')

 <?php
    $offsetTop = rwmb_meta('interiot_offset'); 
    $args = array(  
        'post_type' => 'interjers',
        'post_status' => 'publish',
        'posts_per_page' => 20,
    );

    $loop = new WP_Query( $args ); 
  ?>

    <div class="post-content d-sm-flex">

      <?php while ( $loop->have_posts() ) : $loop->the_post();  ?>
        <?php 
          $offsetTop = rwmb_meta('interiot_offset'); 
        ?>
        <a href="<?php echo get_the_permalink(); ?>">
          <div class="post-item" style="margin-top:<?php echo $offsetTop; ?>px;">
            <div class="post-item__image">
              <img alt="<?php echo the_title(); ?>" src="<?php echo get_the_post_thumbnail_url() ?>"/>
            </div>
            <div class="post-item__content">
              <h2 class="post-item__content__title"><?php echo the_title(); ?></h2>
            </div>
          </div>
        </a>
      <?php endwhile; ?>

    </div>

  <?php
    wp_reset_postdata(); 
 ?>
@endsection


   			