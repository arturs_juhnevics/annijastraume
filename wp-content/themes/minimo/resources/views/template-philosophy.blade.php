{{--
  Template Name: Philosophy
--}}
@extends('layouts.app')
@section('content')

  @while(have_posts()) @php the_post() @endphp
    <h1 class="page-title">Evironments that strengthen life</h1>
    <div class="single-post-item">
     <div class="single-post-item__content single-post-item__content-stories">
        <p>
          <?php
            echo wp_strip_all_tags( get_the_content() );
          ?>
        </p>
     </div>
    </div>
  @endwhile

@endsection
