@extends('layouts.app')
@section('content')

 <?php
    
   
    $args = array(  
        'post_type' => 'dabas-foto',
        'post_status' => 'publish',
        'posts_per_page' => 20,
    );

    $loop = new WP_Query( $args ); 
  ?>

    <div class="post-content d-sm-flex">

      <?php while ( $loop->have_posts() ) : $loop->the_post();  ?>
        <?php 
          $offsetTop = rwmb_meta('naturephoto_offset'); 
          $subtitle = rwmb_meta('naturephoto_subtitle'); 
          $colors = rwmb_meta('naturephoto_color'); 
        ?>
        
          <div class="post-item" style="margin-top:<?php echo $offsetTop; ?>px;">
            <div class="post-item__image">
              <img alt="<?php echo the_title(); ?>" src="<?php echo get_the_post_thumbnail_url() ?>"/>
            </div>
            <div class="post-item__content">
                <div class="post-item__colors">
                    <?php foreach($colors as $color) : ?>
                        <span class="post-item__colors__item" style="background-color: <?php echo $color; ?>"></span>
                    <?php endforeach; ?>
                </div>
                <h2 class="post-item__content__title nature-title"><?php echo the_title(); ?></h2>
                <h2 class="post-item__content__subtitle"><?php echo $subtitle; ?></h2>
            </div>
          </div>
        
      <?php endwhile; ?>

    </div>

  <?php
    wp_reset_postdata(); 
 ?>
@endsection


   			