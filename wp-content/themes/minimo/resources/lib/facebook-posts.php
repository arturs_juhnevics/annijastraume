<?php

class FacebookPosts {

	// WordPress db instance
	public $wpdb;

	// data array
	private $data;

	function __construct(){

		global $wpdb;
		$this->wpdb = $wpdb;
		$this->init();
	}

	public function init(){

		require_once('facebook-posts-init.php');

		$init = new FacebookPostsInit();
		add_action( 'admin_menu', array($init, 'add_menu_page') );

		add_action('wp_ajax_update_facebook', array($this, 'update_facebook'));
		add_action('wp_ajax_nopriv_update_facebook', array($this, 'update_facebook'));
	}

	public function insert( $data, $type ){

	}

	public function curl_helper($url) {
	  $ch = curl_init();
	  $timeout = 5;
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	  curl_setopt($ch, CURLOPT_URL, $url);
	  $data = curl_exec($ch);
	  curl_close($ch);
	  return json_decode($data);
	}

	public function tokenTruncate($string, $your_desired_width) {
	  $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
	  $parts_count = count($parts);

	  $length = 0;
	  $last_part = 0;
	  for (; $last_part < $parts_count; ++$last_part) {
	    $length += strlen($parts[$last_part]);
	    if ($length > $your_desired_width) { break; }
	  }

	  return implode(array_slice($parts, 0, $last_part));
	}


	public function update_facebook(){

		$page_ID = "balticxl";

		$access_token =  "EAAGqRkoVrFwBAE2UvZCjey5YxLZCBVYw3Xe9R04Y3iqEN6HZCbLzBZBUBnchUHEcBHBJLOdsLIBHWz5XFRHdTOAOZBwOIcYjZCw32mmbPfu5qmpD5wC7VyaZC0mk9QeOBavxLGetWPEKoH4qrTqmGtWyquWRP0ZCMowaZBOeklKtyAsok7EpYDpuV";

		$posts = $this->curl_helper('https://graph.facebook.com/'.$page_ID.'/posts?access_token='.$access_token);

		$all_post =  $posts->data;	

		$log_msg = date("Y-m-d h:i:sa") . "\n";
		$count = 1;
		foreach ($all_post as $post) {	

			list($pageID, $postID) = preg_split('/_/', $post->id);
			$post_url = 'https://www.facebook.com/'.$pageID.'/posts'.'/'.$postID;
			$post_image = $this->curl_helper('https://graph.facebook.com/'.$post->id.'/attachments?access_token='.$access_token);
			$title = $this->tokenTruncate($post->message, 50) . " ...";
			$text = $post->message;

			$post_exists = post_exists( $title );

		    if ( !$post_exists ) {
				if (strpos($text, 'youtu.be') == false) {
					if(!empty( $post_image->data[0]->media )){
						$image = $post_image->data[0]->media->image->src;
						if(isset($image) && isset($post->message)){

							$post_id = wp_insert_post(array (
							   'post_type' => 'post',
							   'post_title' => $title,
							   'post_content' => $text,
							   'post_status' => 'publish',
							   'comment_status' => 'closed',   // if you prefer
							));
							wp_set_post_terms( $post_id, array(156), 'category', true);
							$this->insertImage($image, $post_id);

							$log_msg .= $count . '. ' . $title . "\n";

						}
					}
				}
			}
			$count++;
		}

		$logfile = fopen(get_template_directory()."/lib/facebook.log", "a");
		fwrite($logfile, $log_msg);
		fclose($logfile);
	}

	public function insertImage($url, $post_id = null){

			$full_file_name = basename( $url );
			$filename = substr($full_file_name, 0, strpos($full_file_name, "?_nc_cat"));

			// Add Featured Image to Post
			$image_url        = $url; // Define the image URL here
			$upload_dir       = wp_upload_dir(); // Set upload folder
			$image_data       = file_get_contents($image_url); // Get image data
			

			// Check folder permission and define file location
			if( wp_mkdir_p( $upload_dir['path'] ) ) {
			    $file = $upload_dir['path'] . '/' . $filename;
			} else {
			    $file = $upload_dir['basedir'] . '/' . $filename;
			}
			var_dump($file);
			// Create the image  file on the server
			file_put_contents( $file, $image_data );

			// Check image file type
			$wp_filetype = wp_check_filetype( $filename, null );

			// Set attachment data
			$attachment = array(
			    'post_mime_type' => $wp_filetype['type'],
			    'post_title'     => sanitize_file_name( $filename ),
			    'post_content'   => '',
			    'post_status'    => 'inherit'
			);

			// Create the attachment
			$attach_id = wp_insert_attachment( $attachment, $file, $post_id );

			// Include image.php
			require_once(ABSPATH . 'wp-admin/includes/image.php');

			// Define attachment metadata
			$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

			// Assign metadata to attachment
			wp_update_attachment_metadata( $attach_id, $attach_data );

			// And finally assign featured image to post
			set_post_thumbnail( $post_id, $attach_id );

			return $attach_id;
		}

}

new FacebookPosts();