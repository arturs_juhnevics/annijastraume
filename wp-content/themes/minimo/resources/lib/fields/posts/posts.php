<?php
add_filter( 'rwmb_meta_boxes', 'post_meta_boxes' );
function post_meta_boxes( $meta_boxes ) {
     $meta_boxes[] = array(
        'title'      => 'General',
        'post_types' => array('post'),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'    => 'video',
                'name'  => 'Video',
                'type'  => 'oembed',
            ),
             array(
                'id'        => 'feat_post',
                'name'      => 'Featured post',
                'type'      => 'switch',
                
                // Style: rounded (default) or square
                'style'     => 'rounded',

                // On label: can be any HTML
                'on_label'  => 'Yes',

                // Off label
                'off_label' => 'No',
            ),
        ),
    );
    return $meta_boxes;
}