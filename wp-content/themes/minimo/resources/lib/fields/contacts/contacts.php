<?php
add_filter( 'rwmb_meta_boxes', 'contacts_meta_boxes' );
function contacts_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => 'Company details',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-contacts.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'rek_address',
                'name' => 'Address',
                'type' => 'text',
            ),
            array(
                'id'   => 'rek_numurs',
                'name' => 'Registration number',
                'type' => 'text',
            ),
            array(
                'id'   => 'rek_pvn',
                'name' => 'PVN number',
                'type' => 'text',
            ),
            array(
                'id'   => 'rek_bank',
                'name' => 'Bank account',
                'type' => 'text',
            ),
        ),

    );
    $meta_boxes[] = array(
            'title'      => 'Contact form',
            'post_types' => array('page'),
            'include' => array(
                'relation'        => 'OR',
                'template'        => array('views/template-contacts.blade.php')
            ),
            'name' =>'map',
            'context'    => 'normal',
            'priority'   => 'high',
            'fields' => array(

                array(
                    'id'   => 'form_email',
                    'name' => 'Email',
                    'type' => 'text',
                )
            )
        );
    return $meta_boxes;
}