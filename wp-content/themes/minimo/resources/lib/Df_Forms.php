<?php

class Df_Forms {

	// WordPress db instance
	public $wpdb;

	// DB table name
	public $table_name;

	// data array
	private $data;

	function __construct(){

		global $wpdb;
		$this->wpdb = $wpdb;
		$this->table_name = $this->wpdb->prefix . 'df_forms';
		$this->init();
	}

	public function init(){

		require_once('Df_Forms_Init.php');

		$init = new Df_Forms_Init( $this->table_name );
		add_action( 'admin_menu', array($init, 'add_menu_page') );
	}

	public function insert( $data, $type ){

		$this->data = $data;
		$rows_affected = $this->wpdb->insert( $this->table_name,
										array( 'date_created' => current_time('mysql'),
											   'type' => $type,
											   'value' => serialize( $this->data ) ) );
		return $rows_affected;
	}

}

new Df_Forms();