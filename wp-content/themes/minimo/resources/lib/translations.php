<?php

$strings = array(
   
       
    'SIA "BalticXL" Visas tiesības aizsargātas.' => 'General',
    'Cash & carry veikals' => 'General',
    'Seko mums' => 'General',
    'Informācija' => 'General',

    'Adrese' => 'Cash and carry',
    'E-pasts' => 'Cash and carry',
    'Tālrunis' => 'Cash and carry',
    'Faks' => 'Cash and carry',
    'Darbalaiks' => 'Cash and carry',
    'Pirmdiena - Ceturdiena', 'Cash and carry',
    'Piektdiena' => 'Cash and carry',
    'Sestdiena' => 'Cash and carry',
    'Svētdiena' => 'Cash and carry',

    'Biroja adrese' => 'Contacts',
    'Juridiskā adrese' => 'Contacts',
    'Reģistrācijas numurs' => 'Contacts',
    'PVN reģistrācijas numurs' => 'Contacts',
    'Bankas konts' => 'Contacts',

    'Vārds' => 'Contact-form',
    'Telefons' => 'Contact-form',
    'E-pasts' => 'Contact-form',
    'Ziņa' => 'Contact-form',
    'Sūtīt' => 'Contact-form',

    'Uzmanību!' => 'Age check',
    'Vai jums jau ir 18 gadu?' => 'Age check',
    'Jā' => 'Age check',
    'Nē' => 'Age check',

);

if( function_exists('pll_register_string') ) {
    foreach ( $strings as $string => $category ) {
        pll_register_string($string, $string, $category);
    }
}
