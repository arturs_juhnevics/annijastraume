<?php

class NaturePhotos{

    public function __construct() {

        $labels = array(
            'name'               => "Nature Photos",
            'singular_name'      => "Nature Photo",
            'menu_name'          => "Nature Photos",
            'add_new_item'       => "Add new Nature Photo",
            'new_item'           => "New Nature Photo",
            'edit_item'          => "Edit Nature Photo",
            'view_item'          => "View Nature Photo",
            'all_items'          => "All Nature Photos",
            'search_items'       => "Search Nature Photo",
            'not_found'          => "No Nature Photo found",
            'not_found_in_trash' => "No Nature Photo found in trash"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'capabilities' => array(

            ),
            'map_meta_cap' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-portfolio',
            'show_in_rest' => true,
            'supports' => array('thumbnail', 'editor', 'title'),  
            
           
        );

        register_post_type('dabas-foto', $args);

        add_filter( 'rwmb_meta_boxes', 'photos_settings_meta_boxes' );
        function photos_settings_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'naturephoto',
                'title'      => 'Settings',
                'post_types' => array('dabas-foto'),
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(
                    array(
                        'id'    => 'naturephoto_offset',
                        'name' => 'Offset',
                        'type'  => 'number',
                    ),
                    array(
                        'id'    => 'naturephoto_subtitle',
                        'name' => 'Subtitle',
                        'type'  => 'text',
                    ),
                    array(
                        'name'          => 'Color palete',
                        'id'            => 'naturephoto_color',
                        'type'          => 'color',
                        "clone"         => true,
                        // Add alpha channel?
                        'alpha_channel' => true,
                    ),
                )
            );

            return $meta_boxes;
        }


    }

}
new NaturePhotos();