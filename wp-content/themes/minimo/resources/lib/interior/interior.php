<?php

class Interior{

    public function __construct() {

        $labels = array(
            'name'               => "Interior",
            'singular_name'      => "Interior",
            'menu_name'          => "Interior",
            'add_new_item'       => "Add new interior",
            'new_item'           => "New interior",
            'edit_item'          => "Edit interior",
            'view_item'          => "View interior",
            'all_items'          => "All interiors",
            'search_items'       => "Search interior",
            'not_found'          => "No interior found",
            'not_found_in_trash' => "No interior found in trash"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'capabilities' => array(

            ),
            'map_meta_cap' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-portfolio',
            'show_in_rest' => true,
            'supports' => array('thumbnail', 'editor', 'title'),


        );

        register_post_type('interjers', $args);

        add_filter( 'rwmb_meta_boxes', 'interior_settings_meta_boxes' );
        function interior_settings_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'interior',
                'title'      => 'Settings',
                'post_types' => array('interjers'),
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(
                    array(
                        'id'    => 'interiot_offset',
                        'name' => 'Offset',
                        'type'  => 'number',
                    ),
                    array(
                        'id'    => 'interiot_width',
                        'name' => 'Width',
                        'type'  => 'number',
                    ),
                    array(
                        'id'    => 'interiot_info_list',
                        'name'    => 'Info',
                        'type'    => 'key_value',
                        'clone' => true,
                    ),
                    array(
                      'id'               => 'inetriorGallery',
                      'name'             => 'Gallery',
                      'type'             => 'image_advanced',

                      // Delete image from Media Library when remove it from post meta?
                      // Note: it might affect other posts if you use same image for multiple posts
                      'force_delete'     => false,

                      // Do not show how many images uploaded/remaining.
                      'max_status'       => false,

                      // Image size that displays in the edit page. Possible sizes small,medium,large,original
                      'image_size'       => 'thumbnail',
                  ),

                )
            );

            return $meta_boxes;
        }


    }

}
new Interior();
