<?php

class Stories{

    public function __construct() {

        $labels = array(
            'name'               => "Stories",
            'singular_name'      => "Story",
            'menu_name'          => "Stories",
            'add_new_item'       => "Add new Story",
            'new_item'           => "New Story",
            'edit_item'          => "Edit Story",
            'view_item'          => "View Stories",
            'all_items'          => "All Stories",
            'search_items'       => "Search StorY",
            'not_found'          => "No Story found",
            'not_found_in_trash' => "No Stories found in trash"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'capabilities' => array(

            ),
            'map_meta_cap' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-portfolio',
            'show_in_rest' => true,
            'supports' => array('thumbnail', 'editor', 'title'),


        );

        register_post_type('stasti', $args);

        add_filter( 'rwmb_meta_boxes', 'stories_settings_meta_boxes' );
        function stories_settings_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'stories',
                'title'      => 'Settings',
                'post_types' => array('stasti'),
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(
                    array(
                        'id'    => 'stories_offset',
                        'name' => 'Offset',
                        'type'  => 'number',
                    ),
                    array(
                        'id'    => 'stories_subtitle',
                        'name' => 'Subtitle',
                        'type'  => 'text',
                    ),
                    array(
                      'name'   => 'Story Gallery', // Optional
                      'id'     => 'story_gallery',
                      'type'   => 'group',
                      'clone'  => true,
                      // List of sub-fields
                      'fields' => array(
                            array(
                              'id'               => 'story_image',
                              'name'             => 'File Advanced',
                              'type'             => 'file_advanced',

                              // Delete file from Media Library when remove it from post meta?
                              // Note: it might affect other posts if you use same file for multiple posts
                              'force_delete'     => false,

                              // Maximum file uploads.
                              'max_file_uploads' => 1,

                              // File types.
                              // 'mime_type'        => 'application,audio,video',

                              // Do not show how many files uploaded/remaining.
                              'max_status'       => 'false',
                          ),
                          array(
                              'name' => 'Letter',
                              'id'   => 'text',
                              'type' => 'text',
                          ),
                          // Other sub-fields here
                      ),
                    ),

                )
            );

            return $meta_boxes;
        }


    }

}
new Stories();
