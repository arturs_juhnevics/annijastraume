<?php

class FacebookPostsInit{

	public $wpdb;

	public function __construct(){

		global $wpdb;
		global $polylang;

		$this->wpdb = $wpdb;

	}

	public function add_menu_page(){

		add_menu_page( 'Facebook',
			           'Facebook',
			           'edit_posts',
			           'facebook',
			           array( $this, 'render_index' ),
			           null,
			           35 );
	}

	public function render_index(){

		include locate_template( '/views/admin/facebook.php' );

	}

}