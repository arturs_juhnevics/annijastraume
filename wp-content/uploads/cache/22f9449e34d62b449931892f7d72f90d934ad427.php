<?php $__env->startSection('content'); ?>
<?php 
  $name = rwmb_meta('contact_name', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $email = rwmb_meta('contact_email', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $phone = rwmb_meta('contact_phone', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');

  $images = rwmb_meta( 'contact_image', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $image = reset( $images );
  $full_image = $image['full_url'];

?>
  <?php while(have_posts()): ?> <?php the_post() ?>

    <div class="contacts container">
      <div class="row">

      <div class="col-sm-6 d-flex align-items-center justify-content-center">
        <div class="contact-info">
          <p class="contact-info__name">
            <?php echo $name; ?>
          </p>
          <div class="contact-info__details">
            <div class="contact-info__details-key">
              <p  class="contact-info__details-item">dizainere</p>
            </div>
            <div class="contact-info__details-info">
              <a  class="contact-info__details-item contact-info__details-item__link" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
              <a  class="contact-info__details-item contact-info__details-item__link" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="contact-info-image">
          <img alt="<?php echo $name; ?>" src="<?php echo $full_image; ?>"/>
        </div>
      </div>

      </div>
    </div>

  <?php endwhile; ?>
  
<?php $__env->stopSection(); ?>


   			
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>