<?php $__env->startSection('content'); ?>

  <?php while(have_posts()): ?> <?php the_post() ?>
    <h1 class="page-title">Evironments that strengthen life</h1>
    <div class="single-post-item">
     <div class="single-post-item__content single-post-item__content-stories">
        <p>
          <?php
            echo wp_strip_all_tags( get_the_content() );
          ?>
        </p>
     </div>
    </div>
  <?php endwhile; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>