<?php
$logos = rwmb_meta( 'logo', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo = reset( $logos );
$header_image = $logo['full_url'];
$lang_args = array(
	'raw'=>1
);
$estore_url = rwmb_meta( 'e_store_url', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$estore = rwmb_meta( 'enable_estore', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
?>
<header class="header">
<nav class="navigation">

	
	<div class="header__logo-desktop">
		<a class="navbar-brand" href="/"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/logo.svg"); ?></a> 
	</div>
		
	<div class="menu animate">
		<?php if(has_nav_menu('primary_navigation')): ?>
			<?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']); ?>

		<?php endif; ?>
	</div>

</nav>
</header>