<?php
$logos = rwmb_meta( 'logo', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo = reset( $logos );
$header_image = $logo['full_url'];
$classWPbar = "mobile-header";
if ( is_admin_bar_showing() ) {
  $classWPbar = "mobile-header wp-bar";
}
?>
<div class="<?php echo e($classWPbar); ?>">
	<div class="mobile-header__content">
		<div class="header__logo">
			<a class="navbar-logo" href="/">
        <img

            alt="Annija Straume Logo"
            src="<?php echo $header_image; ?>"
          />
      </a>
		</div>
		<button class="hamburger hamburger--squeeze" type="button">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</button>
	</div>
</div>
