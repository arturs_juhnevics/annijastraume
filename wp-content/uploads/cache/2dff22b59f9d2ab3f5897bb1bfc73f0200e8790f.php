<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('layouts.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <?php if(!have_posts()): ?>
    <div class="container">
        <h2 style="text-align: center;">
      <?php echo e(__('Sorry, but the page you were trying to view does not exist.', '404')); ?>

        </h2>
    </div>
  <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>