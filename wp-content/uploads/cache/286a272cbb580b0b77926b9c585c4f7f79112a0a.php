<?php
$heading = rwmb_meta('product_heading'); 
$heading_slug = rwmb_meta('product_slug'); 
$url = rwmb_meta('product_url'); 
$firstPost = get_posts( array(
    'post_type' => 'product',
    'numberposts' => 1,
    'post_status' => 'publish',
    'meta_query' => array(
        array(
            'key'     => 'feat_product',
            'value'   => '1',
            'compare' => 'LIKE',
        ),
    ),
) );
$allPosts = get_posts( array(
    'post_type' => 'product',
    'numberposts' => 5,
    'post_status' => 'publish',
    'meta_query' => array(
        array(
            'key'     => 'feat_product',
            'value'   => '1',
            'compare' => 'LIKE',
        ),
    ),
) );
?>
<div class="container home-section">
	<div class="home-heading-content">
		<div class="home-heading-content__heading animate animate__fade">
			<p class="home-heading-content__slug"><?php echo e($heading_slug); ?></p>
			<h2 class="home-heading-content__title"><?php echo e($heading); ?></h2>
		</div>
		<div class="home-heading-content__button animate animate__fade mob-hidden">
			<a class="button--read-more" href="<?php echo e($url); ?>">VIEW ALL PRODUCTS</a>
		</div>
	</div>
	<div class="home-products">
		<?php
		$count = 0;
		?>
		<div class="row">
			<div class="col-sm-6">
				<?php $__currentLoopData = $firstPost; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php
						$image = get_the_post_thumbnail_url($item->ID);
						$title = get_the_title($item->ID); 
						$url = get_the_permalink($item->ID);

					?>

					<a href="<?php echo e($url); ?>">
						<div class="product-item--large product-item animate animate__fade" style="background-image: url(<?php echo e($image); ?>)">
							<div class="overlay"></div>
							<a class="button-overlay button--read-more">VIEW PRODUCT</a>
							<p class="product-item__title"><?php echo e($title); ?></p>
						</div>
					</a>

				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
			<div class="col-sm-6">
				<div class="row second-row-products">
					<?php $__currentLoopData = $allPosts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php if( $count > 0 ) : //skip first post ?>
						<?php
							$image = get_the_post_thumbnail_url($item->ID);
							$title = get_the_title($item->ID); 
							$url = get_the_permalink($item->ID);

						?>
						<div class="col-sm-6">
							<a href="<?php echo e($url); ?>">
								<div class="product-item--small product-item animate animate__fade" style="background-image: url(<?php echo e($image); ?>)">
									<div class="overlay"></div>
									<div class="button-overlay"><a href="<?php echo e($url); ?>" class="button--read-more">VIEW PRODUCT</a></div>
									<p class="product-item__title"><?php echo e($title); ?></p>
								</div>
							</a>
						</div>
						<?php endif; $count++; ?>

					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		</div> <!-- END row -->
		<div class="button-container mob-only">
			<a href="<?php echo e($url); ?>" class="button">VIEW ALL PRODUCTS</a> 
		</div>
	</div>
</div>