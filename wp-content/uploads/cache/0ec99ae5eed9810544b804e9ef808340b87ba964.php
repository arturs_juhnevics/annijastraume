<?php $__env->startSection('content'); ?>

<?php if ( have_posts() ) : the_post(); ?>
    <?php
    $story_gallery = rwmb_meta('story_gallery');

    ?>
    <div class="single-post-item">
        <div class="single-post-sidebar">
        </div>
        <div class="single-post-content">
          <div class="single-post-gallery">
            <?php foreach ($story_gallery as $item) : ?>
              <div class="single-post-gallery-item">
                <img src="<?php echo wp_get_attachment_image_src($item["story_image"][0], medium)[0]; ?>"/>
                <p class="single-post-gallery-title"><?php echo $item["text"][0]; ?></p>
              </div>
            <?php endforeach; ?>
          </div>
          <div class="single-post-item__content single-post-item__content-stories">
            <?php
              the_content();
            ?>
          </div>
        </div>
    </div>
<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>