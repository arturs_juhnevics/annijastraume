<?php $__env->startSection('content'); ?>

  <?php while(have_posts()): ?> <?php the_post() ?>
    <h1 class="page-title">Evironments that strengthen life</h1>
    <div class="single-post-item">
     <div class="single-post-item__content single-post-item__content-stories">
      <?php echo the_content(); ?>
     </div>
    </div>
  <?php endwhile; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>