<?php 
$logos = rwmb_meta( 'page-header', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo = reset( $logos );
$header_image = $logo['full_url'];
?>
<div class="page-header page-header--simple">
	<?php //breadcrumbs(); ?>
	<div class="container">
		<div class="page-header__title">
			<h1 class="animate animate__fade"><?php echo page_title() ?></h1>
		</div>
	</div>
</div>