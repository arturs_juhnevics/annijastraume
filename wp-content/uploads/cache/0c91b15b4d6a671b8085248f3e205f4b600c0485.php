<?php 
$logos = rwmb_meta( 'page-header', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo = reset( $logos );
$header_image = $logo['full_url'];
?>
<div class="page-header" style="background-image: url(<?php echo e($header_image); ?>);">
	<div class="overlay"></div>
	<?php //breadcrumbs(); ?>
	<div class="page-header__title">
		<h1 class="animate animate__fade-up"><?php echo page_title() ?></h1>
	</div>
</div>