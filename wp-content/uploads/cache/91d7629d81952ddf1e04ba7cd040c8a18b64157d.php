<div class="mobile-header">
	<div class="mobile-header__content">
		<div class="header__logo">
			<a class="navbar-brand" href="/"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/logo.svg"); ?></a> 
		</div>
		<button class="hamburger hamburger--squeeze" type="button">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</button> 
	</div>
</div>