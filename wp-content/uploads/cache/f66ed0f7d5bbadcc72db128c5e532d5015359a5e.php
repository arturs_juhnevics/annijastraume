<?php
$slides = get_posts( array(
    'post_type' => 'slider',
    'numberposts' => -1,
    'post_status' => 'publish',
) );

?>
<div class="hero-wrapper">
	<div class="hero">
	<?php $__currentLoopData = $slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php
		$image = get_the_post_thumbnail_url($slide->ID);

		$title = get_post_meta($slide->ID, 'slider_title');
		$text = get_post_meta($slide->ID, 'slider_text');
		$buttonText = get_post_meta($slide->ID, 'button_text');
		$buttonUrl = get_post_meta($slide->ID, 'button_url');
		$full_width = get_post_meta($slide->ID, 'full_width_bg');
		$full = '';
		if ( $full_width[0] == '1' ) {
			$full = 'full-width';
		}

		?>
		<div class="hero__item animate <?php echo e($full); ?>">
			<div class="overlay"></div>
			<img class="hero__bg" data-lazy="<?php echo e($image); ?>"/>
			<div class="container hero__content ">
	            <div class="hero__item__inner">
	            	<h2 class="hero__title"><?php echo e($title[0]); ?></h2>
	            		<?php if($text): ?>
	                    	<div class="hero__text"><?php echo e($text[0]); ?></div>
	                    <?php endif; ?>
	                    <?php if($buttonUrl): ?>
						<a href="<?php echo e($buttonUrl[0]); ?>" class="button"><?php echo e($buttonText[0]); ?></a> 
						<?php endif; ?>
	           	</div>
	        </div>
		</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</div>
	<div class="slider-dots container mob-hidden "></div>
	<div class="hero-socials desktop-only">
		<div class="container">
			
		
		<?php
        $platforms = array('twitter','youtube', 'facebook', 'linkedin', 'instagram', 'draugiem', 'google');
        ?>
        <ul class="socials animate animate__fade-up">
        	<li><?php echo pll_e('Seko mums', 'General') ?></li>
            <?php foreach ($platforms as $pl) : $_pl = rwmb_meta($pl, array( 'object_type' => 'setting',  'limit' => 1), 'settings');?>
                <?php if(!empty($_pl)) : ?>
                    <li class="social">
                        <a class="soc-item" target="_blank" href="<?php echo esc_url($_pl); ?>">
                        	<span class="social__icon"><?php echo file_get_contents(get_template_directory().'/assets/images/'.$pl.'.svg'); ?></span>                  
                        </a>
                    </li>
                    <?php
                endif;
            endforeach; ?>
        </ul>
        </div>
	</div>
</div>
