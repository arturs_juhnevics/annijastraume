<?php $__env->startSection('content'); ?>

 <?php
    $offsetTop = rwmb_meta('interiot_offset');
    $args = array(
        'post_type' => 'interjers',
        'post_status' => 'publish',
        'posts_per_page' => 20,
    );

    $loop = new WP_Query( $args );
  ?>

    <div class="post-content d-sm-flex">
       <?php $counter = 0 ?>
      <?php while ( $loop->have_posts() ) : $loop->the_post();  ?>
        <?php
          $offsetTop = rwmb_meta('interiot_offset');
          $gallery = rwmb_meta('inetriorGallery', array( 'size' => 'large' )) ;
        ?>
          <div class="post-item <?php echo (($counter % 2 == 0) ? 'low' : 'high') ?>">
            <div class="post-item__gallery-container">
              <div class="post-item__gallery">
                <div class="post-item__gallery-item">
                  <img alt="<?php echo the_title(); ?>" src="<?php echo get_the_post_thumbnail_url() ?>"/>
                </div>
                <?php foreach ( $gallery as $item) { ?>
                  <div class="post-item__gallery-item">
                    <img alt="<?php echo the_title(); ?>" src="<?php echo $item['url'] ?>"/>
                  </div>
                <?php } ?>
              </div>
              <div class="post-item__description" data-id="<?php echo get_the_ID(); ?>">
                <?php echo the_content(); ?>
              </div>
            </div>

            <div class="post-item__content-interior">
              <h2 class="post-item__content-interior__title"><?php echo the_title(); ?></h2>
              <a class="info-button" data-info-id="<?php echo get_the_ID(); ?>">INFO</a>
            </div>
          </div>
          <?php $counter++; ?>
      <?php endwhile; ?>

    </div>

  <?php
    wp_reset_postdata();
 ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>