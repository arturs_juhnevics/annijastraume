<?php
$heading = rwmb_meta('post_heading'); 
$text = rwmb_meta('post_text'); 
$button = rwmb_meta('post_button'); 
$url = rwmb_meta('post_url'); 
$query = new WP_Query( array(
    'post_type' => 'post',
    'numberposts' => 3,
    'post_status' => 'publish',
    'meta_query' => array(
        array(
            'key'     => 'feat_post',
            'value'   => '1',
            'compare' => 'LIKE',
        ),
    ),
) );
?>
<div class="container section" about>
	<div class="home-heading-content">
		<div class="home-heading-content__heading animate animate__fade">
			<h2 class="animate animate__fade-up"><?php echo e($heading); ?></h2>
			
		</div>
		<div class="home-heading-content__button animate animate__fade mob-hidden">
			<a class="button--read-more animate animate__fade-up" href="<?php echo e($url); ?>"><?php echo e($button); ?></a>
		</div>
	</div>
	<p class="about__text animate animate__fade-up"><?php echo e($text); ?></p>
	<div class="posts">

		<?php while($query->have_posts()): ?> <?php $query->the_post() ?>
			<?php echo $__env->make('partials.content-post', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endwhile; ?>

	</div>
	<div class="button-container mob-only">
		<a href="<?php echo e($url); ?>" class="button"><?php echo e($button); ?></a> 
	</div>
</div>