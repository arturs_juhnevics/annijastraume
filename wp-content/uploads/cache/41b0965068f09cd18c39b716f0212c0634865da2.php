<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.page-header-simple', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php

$description = rwmb_meta( 'p_archive_text', array( 'object_type' => 'setting'), 'settings');

$terms = get_categories( 
  array(
    'hide_empty' => true,
  ) 
); 
$blog_page = get_option( 'page_for_posts' );
$object = get_queried_object();

?>

<div class="container posts">

  <div class="post_terms">
    <ul class="post_terms__list">
      <?php 
      $active = '';
      if( $blog_page == $object->ID ) {
        $active = ' active';
      }
      ?>
      <li class="post_terms__list__item animate animate__fade-up<?php echo $active; ?>"><a href='<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>'><?php echo pll__('Visi', 'General'); ?></a></li>
      <?php foreach ($terms as $term) : ?>
        <?php 
        $active = '';
        if( $term->term_id == $object->term_id ) {
          $active = ' active';
        }
        ?>
        <li class="post_terms__list__item animate animate__fade-up<?php echo $active; ?>"><a href='<?php echo get_term_link( $term->term_id ); ?>'><?php echo $term->name; ?></a></li>
      <?php endforeach; ?>
    </ul>
  </div>

  <div class="row">
    <?php while(have_posts()): ?> <?php the_post() ?>
      <div class="col-sm-4">
        <?php echo $__env->make('partials.content-'.get_post_type(), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
    <?php endwhile; ?>
  </div>

  <div class="pager">
  <?php
  global $wp_query;
  $total = $wp_query->max_num_pages;
  $url_params_regex = '/\?.*?$/';
  preg_match($url_params_regex, get_pagenum_link(), $url_params);
  $big = 999999999; // need an unlikely integer

  $base   = str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) );
  $format = 'page/%_%';

     
      $pages = paginate_links( array(
          'base' => $base,
          'format' => $format,
          'current' => max( 1, get_query_var('paged') ),
          'total' => $total,
          'prev_text'          => file_get_contents(get_template_directory().'/assets/images/arrow-left.svg'),
          'next_text'          => file_get_contents(get_template_directory().'/assets/images/arrow-right.svg'),
          'type'  => 'array',
      ) );

      if( $pages ) {

          echo '<ul class="pager__list">';
          foreach ( $pages as $page ) {
              echo '<li class="pager__item">'.$page.'</li>';
          }
          echo '</ul>';
      }
      ?>
  </div><!-- end pager -->

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>