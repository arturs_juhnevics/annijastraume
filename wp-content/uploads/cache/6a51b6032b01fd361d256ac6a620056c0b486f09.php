<?php $__env->startSection('content'); ?>

 <?php
    
   
    $args = array(  
        'post_type' => 'stasti',
        'post_status' => 'publish',
        'posts_per_page' => 20,
    );

    $loop = new WP_Query( $args ); 
  ?>

    <div class="post-content d-flex">

      <?php while ( $loop->have_posts() ) : $loop->the_post();  ?>
        <?php 
          $offsetTop = rwmb_meta('stories_offset'); 
          $subtitle = rwmb_meta('stories_subtitle'); 
        ?>
        <a class="link-wrapper" href="<?php echo get_the_permalink(); ?>">
          <div class="post-item post-item-stories" style="margin-top:<?php echo $offsetTop; ?>px;">
            <div class="post-item__image">
              <img alt="<?php echo the_title(); ?>" src="<?php echo get_the_post_thumbnail_url() ?>"/>
            </div>
            <div class="post-item__content post-item__content-stories">
                <div class="post-item__info">
                    <h2 class="post-item__content__title"><?php echo the_title(); ?></h2>
                    <p class="post-item__content__subtitle"><?php echo $subtitle; ?></p>
                </div>
            </div>
          </div>
        </a>
      <?php endwhile; ?>

    </div>

  <?php
    wp_reset_postdata(); 
 ?>
<?php $__env->stopSection(); ?>


   			
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>