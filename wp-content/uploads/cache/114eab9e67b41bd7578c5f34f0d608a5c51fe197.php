<?php $__env->startSection('content'); ?>
<?php
$info = rwmb_meta('interiot_info_list');  
?>
<?php if ( have_posts() ) : the_post(); ?>
    <div class="single-post-item">

        <div class="single-post-item__intro">
            <div class="single-post-item__image">
                <img alt="<?php echo the_title(); ?>" src="<?php echo get_the_post_thumbnail_url() ?>"/>
            </div>
        </div>

        <div class="single-post-item__content single-post-item__content-stories">
            <?php the_content(); ?>
        </div>
    
    </div>
<?php endif; ?>
<?php $__env->stopSection(); ?>


   			
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>