<?php 
$id = get_the_ID();
$category = get_the_category();
$video = rwmb_meta('video');
$feat_image = get_the_post_thumbnail_url($id, 'large');

?>
<div <?php post_class() ?>>
<div class="container">
  <div class="row">

    <div class="col-sm-5"> 
        <div class="entry-wrapper">
          <div class="entry-content">
            
            <h1 class="page-title"><?php echo get_the_title(); ?></h1>
            
            <?php the_content() ?>
          </div>
        </div>
    </div>

    <div class="col-sm-7"> 
      <?php if ( strpos( $video, 'iframe' ) !== false ) : ?>
        <div class="post-video">
          <?php echo $video; ?>
        </div>
      <?php endif; ?>
      <div class="post-image">
        <img src="<?php echo $feat_image ?>" />
      </div>

    </div>
  
  </div>
  
    <div class="related-posts">
      <div class="related-posts__nav">
        <h3><?php echo pll__('Jums arī varētu interesēt...', 'Post') ?></h3>
        <div class="related-posts__nav__controls slick-controls mob-hidden">
          <span class="arrow-left"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/arrow-left.svg"); ?></span>
          <span class="arrow-right"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/arrow-right.svg"); ?></span>
        </div>
      </div>
      <div class="post-slider">
        <?php 
          $query = new WP_Query( 
            array( 
              'post_type' => 'post',
              'posts_per_page'=> 6, 
              'post__not_in' => array(get_the_ID()),
            ) 
          );
          ?>
          <?php while ($query->have_posts()) : $query->the_post(); ?> 
            <?php echo $__env->make('partials.content-post', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php endwhile; ?>
      </div>
    </div>

  </div>

</div>
