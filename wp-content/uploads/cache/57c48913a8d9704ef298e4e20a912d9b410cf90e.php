<div class="contacts__form">
    <form id="contact-form" method="POST">
        <div class="row">
            <div class="col-sm-6">
                <label class="input__block">
                    <input placeholder="NAME" type="text" name="name" id="name" class="required"/>
                </label>
            </div>
            <div class="col-sm-6">
                <label class="input__block">
                    <input placeholder="EMAIL" type="email" name="email" id="email" class="required"/>
                </label>
            </div>
        </div>
        <div class="row">
          <div class="col-sm-12 ">
               <label class="input__block">
                    <textarea placeholder="MESSAGE" name="message" id="message" ></textarea>
                </label>
            </div>
        </div>
            <div id="recaptcha_element"></div>
            <div class="col-sm-12 button-container" style="text-align: right; padding: 0;">
                <div class="form__status hidden"></div>
                <button class="button" class="contacts__form__submit"><?php echo pll__('Send', 'Contact-form'); ?></button>
            </div>
            <div class="col-sm-12">
                <?php wp_nonce_field('contact-nonce', 'contact-nonce'); ?>
                <?php $from = '';
                if (isset($_GET['from'])) {
                    $from = strip_tags($_GET['from']);
                } ?>
                <input type="hidden" name="page_from" id="page_from" value="<?php echo $from; ?>" class="leave"/>
                <input type="hidden" name="post_id" id="post_id" value="<?php echo $post->ID; ?>" class="leave"/>
                <input type="hidden" name="action" id="action" value="contact_submit" class="leave"/>
                <input type="text" name="contact-honey" id="contact-honey" class="hidden"/>
                
            </div>
    </form>
</div>