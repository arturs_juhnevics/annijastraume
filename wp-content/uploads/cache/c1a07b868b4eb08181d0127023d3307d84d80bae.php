<footer>
	<div class="footer-content">
		<div class="menu animate">
      <?php if(has_nav_menu('primary_navigation')): ?>
        <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']); ?>

      <?php endif; ?>
    </div>
	</div>
</footer>
