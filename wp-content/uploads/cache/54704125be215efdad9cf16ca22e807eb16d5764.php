<?php 
$id = get_the_ID();
$image = get_the_post_thumbnail_url($id, 'medium');
$theme = ($image != false) ? "dark" : "light"; 
$url = get_the_permalink();
$video = rwmb_meta('video');
$category = get_the_category();

?>

<a href="<?php echo e($url); ?>">
<div class="posts__item animate animate__fade">
	<div class="posts__item__image animate animate__fade" style="background-image: url(<?php echo e($image); ?>)">
		
	</div>
	<div class="posts__item__content  <?php echo e($theme); ?>">
		<p class="posts__item__content__category animate animate__fade-up"><?php echo $category[0]->name; ?></p>
		<h3 class="posts__item__content__title animate animate__fade-up"><?php echo get_the_title(); ?></h3>
	</div>
</div>
</a>
