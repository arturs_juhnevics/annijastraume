  <!doctype html>
<html <?php echo get_language_attributes(); ?>>
  <?php echo $__env->make('partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <body <?php body_class() ?>>

  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '468693844077660',
        cookie     : true,
        xfbml      : true,
        version    : 'v5.0'
      });
        
      FB.AppEvents.logPageView();  
       
        
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));


  </script>

    <?php do_action('get_header') ?>
    <?php if( !isset($_COOKIE['age_check'])){ ?>
      <?php echo $__env->make('partials.agecheck', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php } ?>
    <?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="wrapper">

        <div class="main">
          <?php echo $__env->yieldContent('content'); ?>
          <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </main>
      </div>
    <?php do_action('get_footer') ?>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins/rellax.min.js"></script>
     <script async data-main="<?php echo get_template_directory_uri(); ?>/assets/js/app.js?v=1.1.1"
                src="<?php echo get_template_directory_uri(); ?>/assets/js/require.min.js"></script>
    <?php wp_footer() ?>
    <?php echo $__env->make('partials.photoswipe', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</html>
