<?php $__env->startSection('content'); ?>
<div class="container">
	<div class="product-page">
	 	<div class="row">
	 		<div class="col-sm-7">
	 				
	 			<div class="gallery">
					<div class="gallery__main">
						<div class="gallery__main__inner">
							<?php 
								$gallery = rwmb_meta( 'product_gallery', array('size' => 'medium') );
							?>
							<?php foreach ($gallery as $image) { ?>
								<div class="gallery__main__item">
									<img src="<?php echo $image['url'] ?>" />
								</div>
							<?php } ?>
							
						</div>
					</div>
					<?php 
						$gallery = rwmb_meta( 'product_gallery', array('size' => 'thumbnail') );
					?>
					<div class="gallery__thumbs n-visiable mob-hidden">
						<div class="gallery__thumbs__inner clearfix">
							<?php foreach ($gallery as $image) { ?>
								<div class="gallery__thumbs__item">
									<img src="<?php echo $image['url'] ?>" />
								</div>
							<?php } ?>
						</div>
					</div>
				</div>

	 		</div>
	 		<div class="col-sm-5">
	 			<div class="product-info">
	 				<h1 class="animate animate__fade"><?php echo get_the_title(); ?></h1>
	 				<div class="product-info__features animate animate__fade">
	 					<?php 
						$icons = rwmb_meta( 'icons', array('size' => 'thumbnail') );
						?>
						<?php foreach ($icons as $image) { ?>
							<div class="product-info__features__icon">
								<img src="<?php echo $image['url'] ?>" />
							</div>
						<?php } ?>
	 				</div>
	 				<p class="product-info__sdesc animate animate__fade">
	 					<?php 
						$sdesc = rwmb_meta( 'short_description');
						echo $sdesc;
						?>
	 				</p>
	 				<div class="product-info_wholesale animate animate__fade">
	 					<?php 
	 						$title = rwmb_meta( 'p_wholesale_title', array( 'object_type' => 'setting'), 'settings');
	 						$text = rwmb_meta( 'p_wholesale_text', array( 'object_type' => 'setting'), 'settings');
	 						$phone = rwmb_meta( 'p_wholesale_phone', array( 'object_type' => 'setting'), 'settings');
	 						$email = rwmb_meta( 'p_wholesale_email', array( 'object_type' => 'setting'), 'settings');
	 					?>
	 					<div class="content">
	 						<h4><?php echo e($title); ?></h4>
	 						<p><?php echo e($text); ?></p>
	 						<a class="info-icon phone" href="tel:<?php echo e($phone); ?>"><span class="info-icon__icon"><i class="fas fa-phone-square-alt"></i></span><?php echo e($phone); ?></a>
	 						<a class="info-icon email" href="mailto:<?php echo e($email); ?>"><span class="info-icon__icon"><i class="fas fa-envelope-square"></i></span><?php echo e($email); ?></a>
	 					</div>
	 				</div>
	 			</div>
	 		</div>
	 	</div>
	</div>
	<div class="related-posts">
      <div class="related-posts__nav animate animate__fade">
        <h2 class=""><?php echo pll__('More products', 'Product') ?></h2>
        <div class="related-posts__nav__controls slick-controls mob-hidden">
          <span class="arrow-left"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/chevron-left.svg"); ?></span>
          <span class="arrow-right"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/chevron-right.svg"); ?></span>
        </div>
      </div>
      <div class="post-slider">
        <?php 
          $query = new WP_Query( 
            array( 
              'post_type' => 'product',
              'posts_per_page'=> 6, 
              'post__not_in' => array(get_the_ID()),
            ) 
          );
          ?>
          <?php while ($query->have_posts()) : $query->the_post(); ?> 
          <?php 
          	$image = get_the_post_thumbnail_url();
			$title = get_the_title(); 
			$url = get_the_permalink();
          ?>
           <a href="<?php echo e($url); ?>">
				<div class="product-item--medium product-item animate animate__fade" style="background-image: url(<?php echo e($image); ?>)">
					<div class="overlay"></div>
					<div class="button-overlay"><p class="button--read-more">VIEW PRODUCT</p></div>
					<p class="product-item__title"><?php echo e($title); ?></p>
				</div>
			</a>
          <?php endwhile; ?>
      </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>